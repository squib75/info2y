﻿Si scriva un programma IJVM che implementi la seguente funzionalità:
 
- sulla pila sono presenti (inseriti tramite BIPUSH) 'n' valori compresi tra 0 e 127;
 
- il numero 'n' di valori presenti può variare da 1 a 10;
 
- il programma stampa a video soltanto il valore avente la seconda cifra maggiore di tutti gli altri 
(es: il numero 127 ha come prima cifra '1', come seconda cifra '2', come terza cifra '7', mentre il numero '3' ha come prima cifra '0', come seconda cifra '0' e come terza cifra '3');
 
- se non esiste un numero avente la seconda cifra maggiore di tutti gli altri (es: più numeri hanno la stessa seconda cifra), il programma stampa il carattere '-'.


Esempi di funzionamento ([pila in input] -> output a video):


[1] -> 1

[5] -> 5

[94] -> 94

[118] -> 118

[ 76, 76] -> -

[5, 17, 19] -> -

[100, 87, 6, 15, 83, 27] -> -

[100, 87, 6, 15, 93, 27] -> 93

[100, 17, 6, 15, 3, 121, 18] -> 121

[100, 17, 6, 35, 3, 121, 18] -> 35